# Longest Word

Given a text as input, find and output the longest word.

## Sample Input

- this is an awesome text

## Sample Output

- awesome

> Recall the split(' ') method, which returns a list of words of the string.

```py
text = input().split()
length = [len(x) for x in text]
maximum = max(length)
text_index = length.index(maximum)
print(text[text_index])
```
