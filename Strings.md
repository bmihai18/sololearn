# Strings

You need to make a program for a leaderboard.

**The program needs to output the numbers 1 to 9, each on a separate line, followed by a dot:**

- 1.
- 2.
- 3.
- ...

> You can use the \n newline character to create line breaks, or, alternatively, create the desired output using three double quotes """.

```py
print("""1.\n
2.\n
3.\n
4.\n
5.\n
6.\n
7.\n
8.\n
9.""")
```
