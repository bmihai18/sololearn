# SoloLearn

## Python Challengas

- Adding Words
- Average Word Length
- BMI Calculator
- Book Titles
- Celsius to Fahrenheit
- Exponentiation
- Fibonacci
- Flight Time
- Juice Maker
- Letter Frequency
- Longest Word
- Phone Number Validator
- Simple Calculator
- Sum of Consecutive Numbers
- Strings
- Tip Calculator

## Java Challengas

- Binary Converter
- Bowling Game
- Loan Calculator
- Reverse a String
- Shapes
